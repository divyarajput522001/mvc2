﻿namespace ProductWebApplication.MVC.Service
{
    public interface ITokenGenerator
    {
        string GenerateToken(int id, string name);
        bool IsTokenVaild(string userSecretKey,string userIssuer,string userAudience,string userToken);
    }
}
